# Build image : docker build -t demo .
# Run image :  docker run -p 8443:8443 demo 

FROM tomcat
MAINTAINER DI

ADD demo/target/oiosaml2-demo.war /usr/local/tomcat/webapps/oiosaml2-demo.java.war
ADD demo/oiosaml-config /usr/local/tomcat/oiosaml-config
ADD deploy/server.xml /usr/local/tomcat/conf/
ADD deploy/tomcat-users.xml /usr/local/tomcat/conf/

EXPOSE 8443

CMD ["catalina.sh", "run"]
